﻿using Microsoft.EntityFrameworkCore;
using payment_api.Models;

namespace payment_api.Context
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }

        public DbSet<Item> Items { get; set; }

        public DbSet<Seller> Sellers { get; set; }  

        public DbSet<Request> Requests { get; set; }   

        public DbSet<Product> Products { get; set; }
    }
}
