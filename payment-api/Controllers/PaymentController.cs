﻿using Microsoft.AspNetCore.Mvc;
using payment_api.Emum;
using payment_api.Models.DTO;
using payment_api.Services;

namespace payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {        
        private readonly IPaymentService _paymentService;

        public PaymentController(IPaymentService paymentService) {
            
            _paymentService = paymentService;
        }

        [HttpPost]
        public IActionResult RegisterRequest(RequestDTO requestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Dados invalidos!");
            }

            try
            {
                return Ok(_paymentService.RegisterRequest(requestDTO));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }            
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try 
            {
                return Ok(_paymentService.GetById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPatch]
        public IActionResult UpdateStatus(int id, EnumStatus status) 
        {
            try
            {
                return Ok(_paymentService.UpdateStatus(id,status));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
