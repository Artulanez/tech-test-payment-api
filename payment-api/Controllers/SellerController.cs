﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using payment_api.Context;
using payment_api.Models.DTO;
using payment_api.Services;

namespace payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]    
    public class SellerController : ControllerBase
    {        
        private readonly ISellerServices _sellerServices;

        public SellerController(ISellerServices sellerServices) 
        {             
            _sellerServices = sellerServices;
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return Ok(_sellerServices.GetById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpPost]
        public IActionResult Create(SellerDTO sellerDTO)
        {
            if (!ModelState.IsValid) 
            {
                return BadRequest("Dados invalidos!");
            }
            
            try
            {
                return Ok(_sellerServices.Create(sellerDTO));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }         
        }
               
        [HttpPut("{id}")]
        public IActionResult Update(int id, SellerDTO sellerDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Dados invalidos!");
            }            

            try
            {
                return Ok(_sellerServices.Update(id, sellerDTO));
            }
            catch (Exception ex)
            {
                return BadRequest("Erro ao salvar dados: " + ex.Message);
            }            
        }

        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _sellerServices.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }
    }
}
