﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace paymentapi.Migrations
{
    /// <inheritdoc />
    public partial class AjusteTabelas3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtis_Requests_RequestId",
                table: "Produtis");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Produtis",
                table: "Produtis");

            migrationBuilder.RenameTable(
                name: "Produtis",
                newName: "Items");

            migrationBuilder.RenameIndex(
                name: "IX_Produtis_RequestId",
                table: "Items",
                newName: "IX_Items_RequestId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Items",
                table: "Items",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Price = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_products", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Requests_RequestId",
                table: "Items",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Requests_RequestId",
                table: "Items");

            migrationBuilder.DropTable(
                name: "products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Items",
                table: "Items");

            migrationBuilder.RenameTable(
                name: "Items",
                newName: "Produtis");

            migrationBuilder.RenameIndex(
                name: "IX_Items_RequestId",
                table: "Produtis",
                newName: "IX_Produtis_RequestId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Produtis",
                table: "Produtis",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtis_Requests_RequestId",
                table: "Produtis",
                column: "RequestId",
                principalTable: "Requests",
                principalColumn: "Id");
        }
    }
}
