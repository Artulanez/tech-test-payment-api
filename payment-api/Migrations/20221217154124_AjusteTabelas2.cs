﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace paymentapi.Migrations
{
    /// <inheritdoc />
    public partial class AjusteTabelas2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Total",
                table: "Requests",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Total",
                table: "Requests");
        }
    }
}
