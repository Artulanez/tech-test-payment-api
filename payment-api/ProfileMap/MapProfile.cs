﻿using AutoMapper;
using payment_api.Models;
using payment_api.Models.DTO;

namespace payment_api.ProfileMap
{
    public class MapProfile : Profile
    {
        public MapProfile() 
        {
            CreateMap<SellerDTO, Seller>();
            CreateMap<Seller,SellerResultDTO>();

            CreateMap<ProductDTO, Product>();
            CreateMap<Product, ProductResultDTO>();

            CreateMap<RequestDTO, Request>();
            CreateMap<Request, RequestResultDTO>().ForMember(item => item.Items, map => map.MapFrom(src => src.Items));

            CreateMap<ItemDTO, Item>();
            CreateMap<Item, ItemResultDTO>();
        }
    }
}
