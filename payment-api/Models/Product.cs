﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace payment_api.Models
{
    public class Product
    {        
        public int Id { get; set; }
        [NotNull]
        [JsonRequired]
        public string Description { get; set; }
        [NotNull]
        [JsonRequired]
        public double Price { get; set; }
    }
}
