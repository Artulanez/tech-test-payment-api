﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace payment_api.Models
{
    public class Item
    {
        public int Id { get; set; }
        [NotNull]
        [ForeignKey("Product")]
        public int productId{ get; set; }                
        [NotNull]
        public int Amount { get; set; }
    }
}
