﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using payment_api.Emum;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Reflection.Metadata.Ecma335;
using System.Text.Json.Serialization;

namespace payment_api.Models
{
    public class Request
    {
        public int Id { get; set; }
        [NotNull]
        [ForeignKey("Seller")]
        public int SellerId { get; set; }        
        [NotNull]
        public List<Item> Items { get; set; }        
        public double Total { get; set; }
        public DateTime DateRequest { get; set; }
        [NotNull]        
        public EnumStatus Status { get; set; }

    }
}
