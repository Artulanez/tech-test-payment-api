﻿using System.Diagnostics.CodeAnalysis;

namespace payment_api.Models.DTO
{
    public class SellerResultDTO
    {
        public int Id { get; set; }
        [NotNull]
        public string CPF { get; set; }
        [NotNull]
        public string Name { get; set; }
        [NotNull]
        public string Email { get; set; }
        [NotNull]
        public string Phone { get; set; }
    }
}
