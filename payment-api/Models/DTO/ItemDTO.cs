﻿using Microsoft.Build.Framework;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace payment_api.Models.DTO
{
    public class ItemDTO
    {        
        [NotNull]
        [Required]
        [ForeignKey("Product")]
        public int productId { get; set; }        
        [NotNull]
        public int Amount { get; set; }
    }
}
