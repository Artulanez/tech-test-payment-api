﻿using payment_api.Emum;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace payment_api.Models.DTO
{
    public class RequestDTO
    {        
        [NotNull]
        [ForeignKey("Seller")]
        public int SellerId { get; set; }        
        [NotNull]
        public List<ItemDTO> Items { get; set; }        
        [NotNull]
        public EnumStatus Status { get; set; }
    }
}
