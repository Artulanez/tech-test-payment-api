﻿using payment_api.Emum;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace payment_api.Models.DTO
{
    public class RequestResultDTO
    {
        public int Id { get; set; }
        [NotNull]
        public int SellerId { get; set; }
        [NotNull]
        public List<ItemResultDTO> Items { get; set; }
        public double Total { get; set; }
        public DateTime DateRequest { get; set; }  
        [NotNull]
        public EnumStatus Status { get; set; }


    }
}
