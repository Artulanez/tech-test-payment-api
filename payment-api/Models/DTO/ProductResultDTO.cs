﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace payment_api.Models.DTO
{
    public class ProductResultDTO
    {
        public int Id { get; set; }
        [NotNull]
        [JsonRequired]
        public string Description { get; set; }
        [NotNull]
        [JsonRequired]
        public double Price { get; set; }
    }
}
