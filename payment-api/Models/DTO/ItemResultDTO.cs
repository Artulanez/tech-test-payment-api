﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace payment_api.Models.DTO
{
    public class ItemResultDTO
    {
        public int Id { get; set; }
        [NotNull]
        public int productId { get; set; }
        [NotNull]
        public int Amount { get; set; }
    }
}
