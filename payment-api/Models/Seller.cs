﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace payment_api.Models
{
    
    public class Seller
    {        
        public int Id { get; set; }
        [NotNull]
        public string CPF { get; set; }
        [NotNull]
        public string Name { get; set; }
        [NotNull]
        public string Email { get; set; }
        [NotNull]
        public string Phone { get; set; }
    }
}
