﻿using AutoMapper;
using payment_api.Context;
using payment_api.Models;
using payment_api.Models.DTO;

namespace payment_api.Services
{
    public class SellerServices : ISellerServices
    {
        private readonly PaymentContext _context;
        private readonly IMapper _mapper;
        public SellerServices(PaymentContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public SellerResultDTO Create(SellerDTO sellerDTO)
        {            
            try
            {
                Seller seller = _mapper.Map<Seller>(sellerDTO);
                _context.Sellers.Add(seller);
                _context.SaveChanges();
                return _mapper.Map<SellerResultDTO>(seller);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar dados: " + ex.Message);
            }            
        }

        public void Delete(int id)
        {
            var seller = _context.Sellers.Find(id);

            if (seller == null)
            {
                throw new ArgumentException($"Vendedor {id} não encontrado");
            }

            if (_context.Requests.Any(x => x.SellerId == seller.Id))
            {
                throw new ArgumentException($"Vendedor {id} possui registros de vendas, não é possivel excluir.");
            }

            try
            {
                _context.Sellers.Remove(seller);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar dados: " + ex.Message);
            }
        }

        public SellerResultDTO GetById(int id)
        {
            var seller = _context.Sellers.Find(id);

            if (seller == null)
            {
                throw new ArgumentException($"Vendedor {id} não encontrado");
            }

            return _mapper.Map<SellerResultDTO>(seller);
        }

        public SellerResultDTO Update(int id, SellerDTO sellerDTO)
        {
            var sellerBase = _context.Sellers.Find(id);

            if (sellerBase == null)
            {
                throw new ArgumentException($"Vendedor {id} não encontrado");
            }

            var seller = _mapper.Map<Seller>(sellerDTO);
            seller.Id = id;

            try
            {
                _context.Sellers.Update(sellerBase);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar dados: " + ex.Message);
            }

            return _mapper.Map<SellerResultDTO>(seller);
        }
    }
}
