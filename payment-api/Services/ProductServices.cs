﻿using AutoMapper;
using payment_api.Context;
using payment_api.Models;
using payment_api.Models.DTO;

namespace payment_api.Services
{
    public class ProductServices : IProductServices
    {
        private readonly PaymentContext _context;
        private readonly IMapper _mapper;

        public ProductServices(PaymentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ProductResultDTO Create(ProductDTO productDTO)
        {
            try
            {
                Product product = _mapper.Map<Product>(productDTO);
                _context.Products.Add(product);
                _context.SaveChanges();
                return _mapper.Map<ProductResultDTO>(product);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar dados: "+ex.Message);
            }
        }

        public void Delete(int id)
        {
            Product productBase = _context.Products.Find(id);

            if (productBase == null)
            {
                throw new ArgumentException($"Produto {id} não encontrado");
            }

            if (_context.Items.Any(x => x.productId == productBase.Id))
            {
                throw new ArgumentException($"Produto {id} possui registros de vendas, não é possivel excluir.");
            }

            try
            {
                _context.Products.Remove(productBase);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar dados: " + ex.Message);
            }            
        }

        public ProductResultDTO GetById(int id)
        {
            Product product = _context.Products.Find(id);
            if (product == null)
            {
                throw new ArgumentException($"Produto {id} não encontrado");
            }
            return _mapper.Map<ProductResultDTO>(product);
        }

        public ProductResultDTO Update(int id, ProductDTO productDTO)
        {
            Product productBase = _context.Products.Find(id);

            if (productBase == null)
            {
                throw new ArgumentException($"Produto {id} não encontrado");
            }

            try
            {
                productBase = _mapper.Map<Product>(productDTO);
                productBase.Id = id;
                _context.Products.Update(productBase);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar dados: " + ex.Message);
            }

            return _mapper.Map<ProductResultDTO>(productBase);
        }
    }
}
