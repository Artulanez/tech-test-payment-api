﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using payment_api.Context;
using payment_api.Emum;
using payment_api.Models;
using payment_api.Models.DTO;
using System.Data;

namespace payment_api.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly PaymentContext _context;
        private readonly IMapper _mapper;

        public PaymentService(PaymentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public RequestResultDTO GetById(int id)
        {
            var request = _context.Requests.Find(id);

            if (request == null)
            {
                return null;
            }
            return _mapper.Map<RequestResultDTO>(request);
        }

        public RequestResultDTO RegisterRequest(RequestDTO requestDTO)
        {
            double total = 0;
            var seller = _context.Sellers.Find(requestDTO.SellerId);

            if (seller == null)
            {
                throw new ArgumentException("Vendedor não encontrato");                
            }

            foreach (ItemDTO item in requestDTO.Items)
            {
                var product = _context.Products.Find(item.productId);

                if (product == null)
                {
                    throw new ArgumentException($"Item {item.productId} não cadastrado! ");                    
                }
                else
                {
                    total += (product.Price * item.Amount);
                }

            }

            requestDTO.Status = EnumStatus.AguardandoPagamento;
            var request = _mapper.Map<Models.Request>(requestDTO);

            try
            {
                request.Id = 0;
                request.DateRequest = DateTime.Now;
                request.Total = total;


                _context.Requests.Add(request);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Salvar dados: " + ex.Message);                
            }

            return _mapper.Map<RequestResultDTO>(request);
        }

        public RequestResultDTO UpdateStatus(int id, EnumStatus status)
        {
            var requestBase = _context.Requests.Find(id);
            if (requestBase == null)
            {
                throw new ArgumentException($"Venda não com {id} não encontrada!");                
            }


            if ( ( (requestBase.Status == EnumStatus.AguardandoPagamento) &&  (status != EnumStatus.Cancelada && status != EnumStatus.PagamentoAprovado) ) ||
                 ( (requestBase.Status == EnumStatus.PagamentoAprovado) && (status != EnumStatus.Cancelada && status != EnumStatus.EnviadoParaTransportadora) ) || 
                 ( (requestBase.Status == EnumStatus.EnviadoParaTransportadora) && (status != EnumStatus.Entregue) ) )
            {
                throw new ArgumentException($"O pedido esta com status de {requestBase.Status.ToString()} não possivel ir para o Status {status.ToString()} ");
            }

            requestBase.Status = status;
            try
            {
                _context.Requests.Update(requestBase);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Salvar dados: " + ex.Message);
            }

            return _mapper.Map<RequestResultDTO>(requestBase);
        }
    }
}
