﻿using payment_api.Models.DTO;

namespace payment_api.Services
{
    public interface ISellerServices
    {
        public SellerResultDTO Create(SellerDTO sellerDTO);
        public SellerResultDTO GetById(int id);
        public SellerResultDTO Update(int id, SellerDTO sellerDTO);
        public void Delete(int id);
    }
}
