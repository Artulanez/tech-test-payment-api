﻿using payment_api.Models.DTO;

namespace payment_api.Services
{
    public interface IProductServices
    {
        public ProductResultDTO Create(ProductDTO productDTO);
        public ProductResultDTO GetById(int id);
        public ProductResultDTO Update(int id, ProductDTO productDTO);
        public void Delete(int id);
    }
}
