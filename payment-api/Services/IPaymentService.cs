﻿using Microsoft.AspNetCore.Mvc;
using payment_api.Emum;
using payment_api.Models.DTO;

namespace payment_api.Services
{
    public interface IPaymentService
    {
        public RequestResultDTO RegisterRequest(RequestDTO requestDTO);
        public RequestResultDTO GetById(int id);
        public RequestResultDTO UpdateStatus(int id, EnumStatus status);
    }
}
