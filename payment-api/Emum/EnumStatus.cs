﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace payment_api.Emum
{
    public enum EnumStatus
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento = 1,
        [Description("Pagemento Aprovado")]
        PagamentoAprovado = 2,
        [Description("Enviado Para Transportadora")]
        EnviadoParaTransportadora = 3,
        [Description("Entregue")]
        Entregue = 4,
        [Description("Cancelado")]        
        Cancelada = 5
    } 
}
